locals {
  addprefix = flatten([for middleware in var.middlewares : [
    contains(keys(middleware), "addprefix") == false ? [] : flatten([
      {
        key   = "traefik.http.middlewares.${lookup(middleware, "name")}.addprefix.prefix",
        value = lookup(middleware, "addprefix", {}).prefix
      },
    ]),
  ]])
}