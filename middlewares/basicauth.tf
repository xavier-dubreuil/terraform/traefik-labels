locals {
  basicauth = flatten([for middleware in var.middlewares : [
    contains(keys(middleware), "basicauth") == false ? [] : flatten([
      contains(keys(lookup(middleware, "basicauth")), "users") == false ? [] : flatten([
        {
          key   = "traefik.http.middlewares.${lookup(middleware, "name")}.basicauth.users",
          value = join(",", [for username, password in lookup(lookup(middleware, "basicauth"), "users") : "${username}:${password}"])
        }
      ]),
      contains(keys(lookup(middleware, "basicauth")), "usersfile") == false ? [] : flatten([
        {
          key   = "traefik.http.middlewares.${lookup(middleware, "name")}.basicauth.usersfile",
          value = lookup(lookup(middleware, "basicauth"), "usersfile")
        }
      ]),
      contains(keys(lookup(middleware, "basicauth")), "realm") == false ? [] : flatten([
        {
          key   = "traefik.http.middlewares.${lookup(middleware, "name")}.basicauth.realm",
          value = lookup(lookup(middleware, "basicauth"), "realm")
        }
      ]),
      contains(keys(lookup(middleware, "basicauth")), "headerfield") == false ? [] : flatten([
        {
          key   = "traefik.http.middlewares.${lookup(middleware, "name")}.basicauth.headerfield",
          value = lookup(lookup(middleware, "basicauth"), "headerfield")
        }
      ]),
      contains(keys(lookup(middleware, "basicauth")), "removeheader") == false ? [] : flatten([
        {
          key   = "traefik.http.middlewares.${lookup(middleware, "name")}.basicauth.removeheader",
          value = toString(lookup(lookup(middleware, "basicauth"), "removeheader"))
        }
      ]),
    ]),
  ]])
}
