variable "middlewares" {
  type    = any
  default = []
}

output "labels" {
  value = flatten(concat(
    local.addprefix,
    local.basicauth
  ))
}
