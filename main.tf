variable "routers" {
  type    = any
  default = []
}

variable "services" {
  type    = any
  default = []
}

variable "middlewares" {
  type    = any
  default = []
}

module "services" {
  source   = "./services"
  services = var.services
}

module "routers" {
  source  = "./routers"
  routers = var.routers
}

module "middlewares" {
  source      = "./middlewares"
  middlewares = var.middlewares
}

locals {
  globals = [
    { key = "traefik.enable", value = "true" }
  ]
}

output "labels" {
  value = concat(local.globals, module.services.labels, module.routers.labels, module.middlewares.labels)
}
