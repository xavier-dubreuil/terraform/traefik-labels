variable "services" {
  type    = any
  default = []
}

output "labels" {
  value = flatten([for service in var.services : [
    contains(keys(service), "loadbalancer") == false ? [] : flatten([
      contains(keys(lookup(service, "loadbalancer")), "server") == false ? [] : flatten([
        contains(keys(lookup(lookup(service, "loadbalancer"), "server")), "port") == false ? [] : flatten([
          {
            key   = "traefik.http.services.${lookup(service, "name")}.loadbalancer.server.port",
            value = lookup(lookup(lookup(service, "loadbalancer"), "server"), "port")
          },
        ]),
        contains(keys(lookup(lookup(service, "loadbalancer"), "server")), "scheme") == false ? [] : flatten([
          {
            key   = "traefik.http.services.${lookup(service, "name")}.loadbalancer.server.scheme",
            value = lookup(lookup(lookup(service, "loadbalancer"), "server"), "scheme")
          },
        ]),
      ]),
    ]),
  ]])
}