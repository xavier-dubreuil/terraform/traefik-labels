variable "routers" {
  type    = any
  default = []
}

output "labels" {
  value = flatten([for router in var.routers : [
    contains(keys(router), "entrypoints") == false ? [] : flatten([
      { key = "traefik.http.routers.${lookup(router, "name")}.entrypoints", value = join(",", lookup(router, "entrypoints")) },
    ]),
    contains(keys(router), "rule") == false ? [] : flatten([
      { key = "traefik.http.routers.${lookup(router, "name")}.rule", value = lookup(router, "rule") },
    ]),
    contains(keys(router), "service") == false ? [] : flatten([
      { key = "traefik.http.routers.${lookup(router, "name")}.service", value = lookup(router, "service") },
    ]),
    contains(keys(router), "priority") == false ? [] : flatten([
      { key = "traefik.http.routers.${lookup(router, "name")}.priority", value = lookup(router, "priority") },
    ]),
    contains(keys(router), "tls") == false ? [] : flatten([
      { key = "traefik.http.routers.${lookup(router, "name")}.tls", value = "true" },
      contains(keys(lookup(router, "tls")), "certresolver") == false ? [] : flatten([
        { key = "traefik.http.routers.${lookup(router, "name")}.tls.certresolver", value = lookup(lookup(router, "tls"), "certresolver") }
      ]),
    ]),
    contains(keys(router), "middlewares") == false ? [] : flatten([
      { key = "traefik.http.routers.${lookup(router, "name")}.middlewares", value = join(", ", lookup(router, "middlewares")) },
    ]),
  ]])
}